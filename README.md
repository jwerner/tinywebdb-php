TinyWebDB - PHP
===============

This is a PHP backend for the App Inventor [TinyWebDB component](http://ai2.appinventor.mit.edu/reference/components/storage.html#TinyWebDB).

The key/value items are stored in an SQLite database.

## Installation

* Clone this repository
* Install _composer_ dependencies:

    composer install

* If applicable, change `www/.htaccess` for the line:

    RewriteBase /test/tinywebdb/www

... where _/test/tinywebdb/www_  needs to be replaced with your base path on the webserver.

## Components Used

* [Slim Framework: micro framework for PHP 5](http://www.slimframework.com)
* [Paris/Idiorm: lightweight Active Record implementation for PHP5](https://github.com/j4mie/paris)

## API

### Get an overview page:

    GET index.php

A webpage with a table with existing keys/values and links is shown

### Get a list of all tags:

    GET tags

Returns a JSON list of all tags.

### Store a value:

    POST /storeavalue

Post fields:

* _tag:_ The tag for the item
* _value:_ The value for the item
* _entry_key_string:_ Always 0
* _fmt:_ required response format. _html_ or other for JSON

### Retrieve a value:

    POST index.php

Post fields:

* _tag:_ The tag for the item
* _fmt:_ required response format. _html_ or other for JSON

### Delete a value:

    POST deleteentry

Post fields:

* _tag:_ The tag for the item
* _fmt:_ required response format. _html_ or other for JSON

### Delete an entry (2)

    POST storeavalue

Post fields:

* _tag:_ The tag for the item to be deleted
* _value:_ `_DELETE_THIS_ENTRY_`
* _fmt:_ required response format. _html_ or other for JSON

## TODO

* Page footer: make Start link work for all actions
* Delete a value: implement deleting http... key entries (delete URL and all sub paths)

