<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            body {margin-left: 5% ; margin-right: 5%; margin-top: 0.5in;
                 font-family: verdana, arial,"trebuchet ms", helvetica, sans-serif;}
            ul {list-style: disc;}
        </style>
        <title>Tiny WebDB</title>
    </head>
    <body>
        <h1>App Inventor for Android: Custom Tiny WebDB Service</h1>
<?=$content_for_layout; ?>
        <footer>
            <hr />
            <a href="<?php echo $GLOBALS['app']->request->getRootUri() ?>">Start</a> | 
            <a href="https://bitbucket.org/jwerner/tinywebdb-php" target="_blank">jwerner/tinywebdb-php</a>
        </footer>
    </body>
</html>
