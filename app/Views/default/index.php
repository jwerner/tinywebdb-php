<table border=0>
<tr valign="top">
<td><image src="images/customLogo.gif" width="200" hspace="10"></td>
<td>
<p>
This web service is designed to work with <a
href="http://appinventor.mit.edu/explore/">App Inventor
for Android</a> and the TinyWebDB component. The end-goal of this service is
to communicate with a mobile app created with App Inventor.
</p>
The page your are looking at is 
a web page interface to the web service to help programmers with debugging. You
can invoke the get and store operations by hand, view the existing entries, and also delete individual entries.</p>

</td> </tr> </table>

<h3>Available calls:</h3>
<ul>
    <li><a href="<?php echo $GLOBALS['app']->request->getRootUri() ?>/storeavalue">/storeavalue</a>: Stores a value, given a tag and a value</li>
    <li><a href="<?php echo $GLOBALS['app']->request->getRootUri() ?>/getvalue">/getvalue</a>: Retrieves the value stored under a given tag.  Returns the empty string if no value is stored</li>
</ul>

<h3>Stored Data</h3>

<table border=1>
    <tr>
        <th>Key</th>
        <th>Value</th>
        <th>Created (GMT)</th>
        <th>Action</td>
    </tr>
    <?php foreach($entities as $e) : ?>
    <tr>
        <td><a href="<?php echo $GLOBALS['app']->request->getRootUri() ?>/storeavalue/<?php echo $e->key ?>" title="View this key/value"><?php echo htmlentities($e->tag); ?></a></td>
        <td><?php echo htmlentities($e->value); ?></td>
        <td><?php echo date("Y-m-d H:i:s", $e->timestamp); ?></td>
        <td>
            <form action="<?php echo $GLOBALS['app']->request->getRootUri() ?>/deleteentry" method="post"
                enctype="application/x-www-form-urlencoded"
                onsubmit="return confirm('\n' + 'Confirm to really delete this key: \n\n' + '<?php echo $e->tag ?>' + '\n\n');">
                <input type="hidden" name="entry_key_string" value="<?php echo $e->key ?>">
	            <input type="hidden" name="tag" value="<?php echo $e->tag ?>">
                <input type="hidden" name="fmt" value="html">
                <input type="submit" style="background-color: red" value="Delete" title="Delete this key/value">
            </form>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
