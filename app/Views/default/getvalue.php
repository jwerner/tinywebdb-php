<h2>Get Value</h2>
<form action="<?php echo $GLOBALS['app']->request->getRootUri() ?>/getvalue" method="post"
    enctype=application/x-www-form-urlencoded>
    <p>Tag: <input type="text" name="tag" value="<?php echo $tag ?>" /></p>
    <input type="hidden" name="fmt" value="html">
    <input type="submit" value="Get value">
</form>


<?php if(count($entities)>0) : ?>

<hr />
<em>The server will send this to the component:</em><br />
<pre>
<?php 
if(count($entities)==1) {
    echo json_encode(array('VALUE', $tag, $entities[0]->value));
} else {
    $values = array();
    foreach($entities as $e) {
        $values[] = $e->value;
    }
    echo json_encode(array('VALUE', $tag, $values));
} ?></pre>
<h3>Results</h3>
<table border=1>
    <tr>
        <th>Key</th>
        <th>Value</th>
        <th>Created (GMT)</th>
        <th>Action</td>
    </tr>
    <?php foreach($entities as $e) : ?>
    <tr>
        <td><a href="<?php echo $GLOBALS['app']->request->getRootUri() ?>/storeavalue/<?php echo $e->key ?>"><?php echo htmlentities($e->tag); ?></a></td>
        <td><?php echo htmlentities($e->value); ?></td>
        <td><?php echo date("Y-m-d H:i:s", $e->timestamp); ?></td>
        <td>
            <form action="<?php echo $GLOBALS['app']->request->getRootUri() ?>/deleteentry" method="post"
                enctype=application/x-www-form-urlencoded>
                <input type="hidden" name="entry_key_string" value="<?php echo $e->key ?>">
	            <input type="hidden" name="tag" value="<?php echo $e->tag ?>">
                <input type="hidden" name="fmt" value="html">
                <input type="submit" style="background-color: red" value="Delete">
            </form>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php else : ?>
<p>
No tags found.
</p>
<?php endif; ?>
