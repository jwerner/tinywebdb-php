<h2>Store A Value</h2>

<form action="<?php echo $GLOBALS['app']->request->getRootUri() ?>/storeavalue" method="post"
    enctype=application/x-www-form-urlencoded>
    <p>Tag:<br />
        <input type="text" name="tag" value="<?php echo $e->tag ?>" />
    </p>
    <p>Value:<br />
        <textarea name="value" cols="30" rows="5"><?php echo $e->value ?></textarea>
    </p>
    <input type="hidden" name="entry_key_string" value="<?php echo $e->key ?>">
    <input type="hidden" name="fmt" value="html">
    <input type="submit" value="Store a value">
</form>
<script type="text/javascript">
<!--
// Set focus to tag input:
document.getElementsByName("tag")[0].focus();
document.getElementsByName("tag")[0].select();
// -->
</script>
