<?php
	
$app->get('/', function() use ($app) { // {{{ 
    $entities = Model::factory('Entity')->order_by_expr('tag')->find_many(); 
 	return $app->render('default/index.php', array( 
        'name' => 'World!',
        'entities' => $entities
	));
 
}); // }}} 
$app->get('/storeavalue(/:key)', function($key=0) use ($app) { // {{{ 
    if($key==0) {
        $entity = Model::factory('Entity')->create();
        $entity->key = 0;
    } else { 
        $entity = Model::factory('Entity')->find_one($key);
    }
 	return $app->render('default/storeavalue.php', array( 
        'e'=>$entity
    ));
}); // }}} 
$app->post('/storeavalue', function() use ($app) { // {{{ 
    $key            = $app->request->post('entry_key_string');
    $tag            = $app->request->post('tag');
    $value          = $app->request->post('value');
    $format         = $app->request->post('fmt');
    
    // remove leading/trailing quotes
    if(substr($tag, 0, 1)==='"')
        $tag = substr($tag, 1);
    if(substr($tag, -1)==='"')
        $tag = substr($tag, 0, -1);

    $bUpdate = false;
    $entity = Model::factory('Entity')->where('tag', $tag)->find_one();
    if($entity instanceof Entity) {
        // We found the entry
        $bUpdate = true;
    } else {
        $entity = Model::factory('Entity')->create();
    }
        
    if(substr($value, 0, 1)==='"')
        $value = substr($value, 1);
    if(substr($value, -1)==='"')
        $value = substr($value, 0, -1);

    // Delete entry?
    if($bUpdate===true and $value==='_DELETE_THIS_ENTRY_') {
        $entity->delete();
    } else {
        $entity->value  = $value;
        $entity->tag    = $tag;
        $entity->timestamp = time();
        $entity->save();
    }

    if($format=='html') {
        $app->response->redirect('/test/tinywebdb-php/www', 303);
    } else {
        $app->response->headers->set('Content-Type', 'application/json');
        echo json_encode(array("STORED", $tag, $value));
    }

}); // }}} 
$app->post('/deleteentry', function() use ($app) { // {{{ 
    $entry_key_string = $app->request->post('entry_key_string');
    $tag = $app->request->post('tag');
    $format = $app->request->post('fmt');

    $entity = Model::factory('Entity')->find_one($entry_key_string);
    if($entity instanceof Entity) {
        $entity->delete();
        $app->response->redirect('/', 303);
    }

}); // }}} 
$app->get('/tags', function() use ($app) { // {{{ 
    $entities = Model::factory('Entity')->group_by('tag')->order_by_asc('tag')->find_many();
    $tags = array();
    foreach($entities as $e) {
        $tags[] = $e->tag;
    }
    echo json_encode($tags);
    
}); // }}}
$app->get('/getvalue', function() use ($app) { // {{{ 
    return $app->render('default/getvalue.php', array( 
        'tag'=>'',
        'entities' => array(),
    ));
}); // }}} 
$app->post('/getvalue', function() use ($app) { // {{{ 
    $tag = $app->request->post('tag');
    $format = $app->request->post('fmt');
    $entities = Model::factory('Entity')->where('tag', $tag)->find_many();

    if($format=='html') {
        return $app->render('default/getvalue.php', array( 
            'tag'=>$tag, 
            'entities'=>$entities,
        ));
    } else {
        $app->response->headers->set('Content-Type', 'application/json');
        if(count($entities)==1) {
            echo json_encode(array('VALUE', $tag, $entities[0]->value));
        } else {
            $values = array();
            foreach($entities as $e) {
                $values[] = $e->value;
            }
            echo json_encode(array('VALUE', $tag, $values));
        }
    }
}); // }}} 
