<?php
/**
 * A PHP backend for App Inventor's TynyWebDB component
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
require '../vendor/autoload.php';
require '../app/config.php';

// Models
require '../app/Models/Entity.php';

// Setup Paris ORM
ORM::configure("sqlite:../app/data/entities.db");
$db = ORM::get_db();

// Create table 'entity'
$db->exec("
    CREATE TABLE IF NOT EXISTS `entity` (
	    `key`	INTEGER PRIMARY KEY AUTOINCREMENT,
	    `tag`	TEXT NOT NULL,
	    `value`	TEXT NOT NULL,
	    `timestamp`	INTEGER NOT NULL DEFAULT '0'
    );"
);

//  prepare view
$view = new \app\Library\Extensions\LayoutView(array(
    'layout.auto' => true,
    'layout.path' => '../app/Views/layouts/',
    'layout.file' => 'default.php'
));

// Instantiate a Slim application:
$app = new \Slim\Slim(array_merge_recursive(array(
    'templates.path' => '../app/Views',
    'view' => $view
), $config['slim']));

//  load controllers
require '../app/Controllers/DefaultController.php';

// Run the Slim application:
$app->run();
